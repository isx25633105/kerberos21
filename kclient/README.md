# KERBEROS @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/kerberos21:kserver** Servidor kerberos edt.org
* **isx25633105/kerberos21:kclient** client kerberos edt.org

 S'ha fet el següent:
 *  isx25633105/kerberos21:kserver servidor kerberos detach. Crea els principals pere(kpere)
    pau(kpau, rol: admin), jordi(kjordi), anna (kanna), marta (kmarta), marta/admin (kmarta
    rol:admin), julia (kjulia) i admin (kadmin rol:admin). Crear també els principals
    kuser01...kuser06 amb passwd (kuser01...kuser06). Assignar-li el nom de host:
    kserver.edt.org.
 *  isx256331'5/kclient:khost host client de kerberos. Simplement amb eines kinit, klist i kdestroy
    (no pam). El servidor al que contacta s'ha de dir kserver.edt.org. Cal verificar el
    funcionament de kadmin.
```
docker network create 2hisx
docker build -t isx25633105/kerberos21:server .
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d isx25633105/ldap21:group 

docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d edtasixm06/phpldapadmin:20
```
 

