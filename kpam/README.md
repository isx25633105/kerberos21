# kerberos @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/kerberos21:khostp:**
* Ordre per executar el container:
* Xarxa propia:

```

docker build -t isx25633105/kerberos21:khostp .

docker run --privileged --rm --name khostp.edt.og -h khostp.edt.org --net 2hisx -it isx25633105/kerberos21:khostp /bin/bash
```

S'ha de modificar el /etc/hosts i afegir els host ssh.edt.org i el kserver.edt.org.
 
