# KERBEROS @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/kerberos21:kserver** Servidor kerberos edt.org

 S'ha fet el següent:
 *  isx25633105/kerberos21:kserver servidor kerberos detach. Crea els principals pere(kpere)
    pau(kpau, rol: admin), jordi(kjordi), anna (kanna), marta (kmarta), marta/admin (kmarta
    rol:admin), julia (kjulia) i admin (kadmin rol:admin). Crear també els principals
    kuser01...kuser06 amb passwd (kuser01...kuser06). Assignar-li el nom de host:
    kserver.edt.org.
```
docker network create 2hisx
docker build -t isx25633105/kerberos21:server .
docker run --rm --name kserver.edt.org -h kserver.edt.org -p 749:749 -p 88:88 -p 464:464 --net 2hisx isx25633105/kerberos21:kserver 
```
 

