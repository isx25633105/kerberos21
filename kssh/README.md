# KERBEROS @isx25633105 ASIX
# Curs 2021-2022

* **isx25633105/kerberos21:kssh:**
* Ordre per executar el container:
* Xarxa propia:

```

docker build -t isx25633105/kerberos21:kssh .

docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisx -p 2022:22 -d isx25633105/kerberos21:kssh
```

